/* When we are using TASA with nvcc, we want to use only C compliant features, but nvcc is actually using C++ linkage
 * so, we make sure that __BEGIN_DECLS and __END_DECLS are defined before we actully include the system header.
 */

#define __BEGIN_DECLS extern "C" {
#define __END_DECLS }

#include_next <stdio.h>
